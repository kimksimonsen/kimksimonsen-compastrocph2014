; $Id$
PRO write_binary
  ; ---------------------------------------------
  ; Construct an 8x8 data array
  ; ---------------------------------------------
  n=8
  data=fltarr(n,n)
  for i=0,n-1 do for j=0,n-1 do data[i,j]=i+10*j
  print,data,format='(8f7.1)'
  ; ---------------------------------------------
  ; Show the array as an image
  ; ---------------------------------------------
  imsize=512
  window,xs=imsize,ys=imsize
  device,decompose=0
  loadct,39
  tvscl,rebin(data,imsize,imsize),top=254
  ; ---------------------------------------------
  ; Write the data out as unformatted, raw binary
  ; ---------------------------------------------
  openw,1,'binary.dat'
  writeu,1,data
  close,1
END