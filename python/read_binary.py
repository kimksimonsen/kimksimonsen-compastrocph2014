""" Demo: read and show binary data """
from struct import unpack
from numpy import reshape
from matplotlib.pyplot import imshow, show

s = open("binary.dat", "rb").read()
data = unpack("@64f", s)
data = reshape(data,(8,8))
print(data)
imshow(data,origin='lower')
show()