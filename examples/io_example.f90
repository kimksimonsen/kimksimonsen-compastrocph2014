PROGRAM io_example
  USE mpi_coords
  USE mpi_io_mod
  implicit none
  integer, dimension(3):: dd, d, offset, n_mpi
  real, allocatable, dimension(:,:,:):: f
  logical:: ok=.true.
  integer:: rec, rank
  real:: f1
!-------------------------------------------------------------------------------
! Initialize MPI with a cartesian MPI arrangement
!-------------------------------------------------------------------------------
  call init_mpi
  n_mpi(1) = 2                                                                  ! two ranks in x
  n_mpi(2) = mpi_size/n_mpi(1)                                                  ! the rest in y
  n_mpi(3) = 1                                                                  ! no split in z
  call cart_create_mpi (n_mpi)                                                  ! make cartesian
!-------------------------------------------------------------------------------
! Open file for write-read and write unique values
!-------------------------------------------------------------------------------
  dd(1:3) = 32                                                                  ! global dimensions
  dd(3) = 1
  d = dd/n_mpi                                                                  ! our size
  offset = mpi_coord*d                                                          ! our offset
  allocate (f(d(1),d(2),d(3)))                                                  ! allocate array
  call file_openw_mpi ('tmp.dat', dd, d, offset)                                ! open for writing
  do rec=0,9                                                                    ! write 10 records
    f = mpi_rank + 100*rec                                                      ! unique values
    call file_write_mpi (f, d, rec)                                             ! collective write
  end do
  call file_close_mpi
!-------------------------------------------------------------------------------
! Re-open file for reading and check unique values
!-------------------------------------------------------------------------------
  call file_openr_mpi ('tmp.dat', dd, d, offset)                                ! open the same file
  do rec=0,9                                                                    ! 10 records
    call file_read_mpi (f, d, rec)                                              ! read one record
    f1 = mpi_rank + 100*rec                                                     ! ok value
    ok = ok .and. f(1,1,1)==f1                                                  ! check if ok values
   !print'(a,i4,2f9.1,l3)','mpi_rank, f =', mpi_rank, f1, f(1,1,1), ok          ! check unique values
  end do
  print'(a,i4,4x,a)', ' mpi_rank', mpi_rank, merge('OK ','NOK',ok)
  call file_close_mpi
!-------------------------------------------------------------------------------
  deallocate (f)
  call end_mpi
END PROGRAM io_example
