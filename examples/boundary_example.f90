!*******************************************************************************
PROGRAM coords_example
  USE mpi_coords
  implicit none
  integer, save:: n_mpi(3) = (/1,1,1/)
  integer:: rank
!-------------------------------------------------------------------------------
  call init_mpi
  n_mpi(1) = 2
  n_mpi(2) = mpi_size/n_mpi(1)
  call cart_create_mpi (n_mpi)
  if (master) print*,'Finding the nearest neighbors from mpi_dn & mpi_up:'
  call barrier_mpi('1'); call sleep(1)
  do rank=0,mpi_size-1
    if (rank==mpi_rank) &
      print'(a,i4,3(5x,a,3i4))', &
        'rank:', mpi_rank, &
        'pos:' , mpi_coord, &
        'dn:'  , mpi_dn, &
        'up:'  , mpi_up
    call barrier_mpi('2')
  end do
  call end_mpi
END PROGRAM coords_example
