!*******************************************************************************
PROGRAM send_example
  USE mpi_send
  implicit none
  integer, parameter:: nw=100
  real, dimension(nw):: a, b
  integer i, to, from, offset, req(2)
  character(len=mch), save:: id= &
    'rma_example.f90 $Id$'
!-------------------------------------------------------------------------------
  call init_mpi
  call print_id (id)
  call barrier_mpi('init')
!-------------------------------------------------------------------------------
  do i=1,nw
    a(i) = i + nw*mpi_rank
  end do
!-------------------------------------------------------------------------------
  to   = modulo(mpi_rank-1, mpi_size)                                           ! send to one rank down
  from = modulo(mpi_rank+1, mpi_size)                                           ! recv from one rank up
  offset = 10
  call isend_reals_mpi (a(1+offset), nw-offset,   to, mpi_comm_world, req(1))   ! <-  NO change to ..
  call irecv_reals_mpi (b          , nw-offset, from, mpi_comm_world, req(2))   !   | .. a in this ..
  call waitall_mpi (req, 2)                                                     ! <-  .. interval
  print *,mpi_rank,b(1)                                                         ! check results
!-------------------------------------------------------------------------------
  call end_mpi
END PROGRAM send_example
