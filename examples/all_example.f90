!*******************************************************************************
PROGRAM all_example
  USE mpi_all
  implicit none
  integer:: i, rank, ndata=100
  real, allocatable, dimension(:):: send1, recv1
  real, allocatable, dimension(:,:):: send2, recv2
  character(len=mch), save:: id= &
    'all_example.f90 $Id$'
!-------------------------------------------------------------------------------
  call init_mpi
  call print_id (id)
!-------------------------------------------------------------------------------
! Allocate send and receive buffers, and fill the first send buffer
!-------------------------------------------------------------------------------
  allocate (send1(ndata), recv1(ndata))
  allocate (send2(ndata, mpi_size), recv2(ndata, mpi_size))
  do i=1,ndata
  do rank=1,mpi_size
     send2(i,rank) = i+100*rank
  end do
  end do
!-------------------------------------------------------------------------------
! Send ndata values from the rank 0 to all others
!-------------------------------------------------------------------------------
  call scatter_reals_mpi (send2, recv1, ndata, 0, mpi_size, mpi_comm_world)
  print *,'recv1:',mpi_rank, recv1(1:ndata:ndata-1)
  send1 = recv1 + 1
!-------------------------------------------------------------------------------
! Gather ndata values from all ranks to rank 0
!-------------------------------------------------------------------------------
  call  gather_reals_mpi (send1, recv2, ndata, 0, mpi_size, mpi_comm_world)
  if (mpi_rank==0) then
    do rank=1,mpi_size
      print *,'recv2:',mpi_rank,rank,recv2(1:ndata:ndata-1,rank)
    end do
  end if
!-------------------------------------------------------------------------------
! Send different data from each rank to all other ranks
!-------------------------------------------------------------------------------
  do rank=1,mpi_size
  do i=1,ndata
    send2(i,rank) = i + rank*2000
  end do
  end do
  call allgather_reals_mpi (send2, recv2, ndata, 0, mpi_size, mpi_comm_world)
  do rank=1,mpi_size
  do i=1,ndata
    print *,'allgather recv2:',mpi_rank,rank,recv2(1:ndata:ndata-1,rank)
  end do
  end do
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
  call  alltoall_reals_mpi (send2, recv2, ndata, mpi_size, mpi_comm_world)
  do rank=1,mpi_size
  do i=1,ndata
    print *,'alltoall recv2:',mpi_rank,rank,recv2(1:ndata:ndata-1,rank)
  end do
  end do
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!  call alltoallv_reals_mpi (send, send_count, send_offset, &
!                            recv, recv_count, recv_offset, &
!                            count, mpi_comm_world)
  deallocate (send1, recv1, send2, recv2)
  call end_mpi
END PROGRAM all_example
