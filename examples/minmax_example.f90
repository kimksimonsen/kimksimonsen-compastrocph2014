!*******************************************************************************
PROGRAM main
  USE mpi_reduce
  implicit none
  character(len=mch), save:: id= &
    'main.f90 $Id$'
  integer rank, i, m
  integer, parameter:: na=10
  real, dimension(na):: a
!-------------------------------------------------------------------------------
  call init_mpi
  call print_id(id)
  do i=1,na
    a(i) = i+mpi_rank
  end do

  call barrier_mpi('before print')
  if (master) print*,'just print'
  m = mpi_rank
  call max_int_mpi (m)
  call max_reals_mpi (a, na)
  print *,mpi_rank,mpi_size,m,a(na)
  call barrier_mpi('after print')

  if (master) print*,'ordered print'
  call barrier_mpi('before loop')
  do rank=0,mpi_size-1
    m = rank
    call max_int_mpi (m)
    call max_reals_mpi (a, na)
    if (rank==mpi_rank) print*,mpi_rank,mpi_size,m,a(na)
    call barrier_mpi('in loop')
  end do

  call end_mpi
END PROGRAM main
!*******************************************************************************
