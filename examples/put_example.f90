!*******************************************************************************
PROGRAM put_example
  USE mpi_rma
  implicit none
  integer, parameter:: nw=100
  real, dimension(nw):: a, b
  integer i, win, rank, offset
  character(len=mch), save:: id= &
    'put_example.f90 $Id$'
!-------------------------------------------------------------------------------
  call init_mpi
  call print_id (id)
  call barrier_mpi('init')
!-------------------------------------------------------------------------------
  call win_create_mpi (win, b, nw)                                              ! make b available
  do i=1,nw
    a(i) = i + nw*mpi_rank
  end do
!-------------------------------------------------------------------------------
  rank = modulo(mpi_rank-1, mpi_size)                                           ! put to one rank down
  offset = 10
  call win_fence_mpi (win)                                                      ! <-  NO access to ..
  call put_reals_mpi (win, a(1+offset), nw-offset, offset, rank)                 !   | .. a&b in this ..
  call win_fence_mpi (win)                                                      ! <-  .. interval
  print *,mpi_rank,b(1+offset)                                                  ! check results
!-------------------------------------------------------------------------------
  call win_free_mpi (win)
  call end_mpi
END PROGRAM put_example
