!*******************************************************************************
PROGRAM coords_example
  USE mpi_coords
  implicit none
!-------------------------------------------------------------------------------
  call init_mpi
  call cart_create_mpi ((/3,2,1/))
  print*,mpi_rank,mpi_coord,mpi_plane,mpi_beam
  call end_mpi
END PROGRAM coords_example
