! $Id$
!*******************************************************************************
MODULE mpi_base
 !USE mpi                       ! Most systems accept this, but ..
  implicit none 
  include 'mpif.h'              ! .. some systems may require this instead
  integer:: mpi_err             ! MPI error status
  logical:: mpi_master          ! true on MPI master (rank 0)
  logical:: do_trace=.false.    ! debug trace option
PUBLIC
  logical:: master              ! true on OMP master thread on MPI master process
  integer:: mpi_comm            ! short hand for mpi_comm_world
  integer:: mpi_size            ! number of MPI processes
  integer:: mpi_rank            ! process number (zero-based)
  integer, parameter:: mch=120  ! max character string size
  public mpi_comm_world         ! main MPI communicator
  public MPI_INTEGER            ! MPI magic number for integers
  public MPI_INT                ! ditto
  public MPI_REAL               ! for (4-byte) real's
CONTAINS

!*******************************************************************************
SUBROUTINE init_mpi
  implicit none
  character(len=mch):: id='mpi.f90 $Id$'
!-------------------------------------------------------------------------------
!  Start up MPI and get number of nodes and our node rank
!-------------------------------------------------------------------------------
  call MPI_Init (mpi_err)
  mpi_comm = mpi_comm_world
  call MPI_Comm_size (mpi_comm_world, mpi_size, mpi_err)
  call MPI_Comm_rank (mpi_comm_world, mpi_rank, mpi_err)
  mpi_master = (mpi_rank == 0)
  master = mpi_master
  call print_id (id)
END SUBROUTINE init_mpi

!*******************************************************************************
SUBROUTINE end_mpi
  implicit none
!..............................................................................
  call MPI_Finalize (mpi_err)
  call exit        ! avoid "stop" which may produce one output line per process
END SUBROUTINE

!*******************************************************************************
SUBROUTINE abort_mpi
  implicit none
!...............................................................................
  call MPI_Abort (mpi_comm_world, 127, mpi_err)
  call exit
END SUBROUTINE abort_mpi

!*******************************************************************************
SUBROUTINE barrier_mpi (label)
  implicit none
  character(len=*) label
!...............................................................................
  if (do_trace) print *,  mpi_rank, 'barrier_mpi: '//label
  call MPI_Barrier (mpi_comm_world, mpi_err)
END SUBROUTINE barrier_mpi

!*******************************************************************************
REAL(kind=8) FUNCTION wallclock()
  implicit none
  include 'mpif.h'
  real(kind=8):: time
  real(kind=8), save:: offset=-1.
!...............................................................................
  time = MPI_Wtime()
  if (offset == -1.) offset=time
  wallclock = time-offset
END FUNCTION wallclock

!*******************************************************************************
SUBROUTINE print_id (id)
  implicit none
  character(len=mch) id
  character(len=mch), save:: hl= &
    '--------------------------------------------------------------------------------'
!..............................................................................
  if (id .ne. ' ' .or. do_trace) then
    if (master) then
      print'(a)',trim(hl)
      print'(1x,a,f12.2)', trim(id), wallclock()
    end if
    if (.not.do_trace) id = ' '
  end if
END SUBROUTINE print_id

!*******************************************************************************
SUBROUTINE trace_mpi (message)
  implicit none
  character(len=*):: message
!..............................................................................
  if (do_trace) print*,mpi_rank,' trace: '//trim(message)
END SUBROUTINE trace_mpi

!*******************************************************************************
SUBROUTINE assert_mpi (message, code)
  implicit none
  logical:: flag
  character(len=*):: message
  integer code
!..............................................................................
  if (code/=0) &
    print*,mpi_rank,' error: '//trim(message)//', code =', code
END SUBROUTINE assert_mpi

END MODULE mpi_base
