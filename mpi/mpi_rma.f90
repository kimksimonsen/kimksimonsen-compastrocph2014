! $Id$
!*******************************************************************************
MODULE mpi_rma
  USE mpi_base
  implicit none
PUBLIC
CONTAINS

!*******************************************************************************
SUBROUTINE win_create_mpi (win, c, nw)
  implicit none
  integer:: win, nw
  real, dimension(nw):: c
  integer(kind=MPI_ADDRESS_KIND):: nbytes
  !----------------------------------------------------------------------------
  ! Create MPI remote memory window, of size nb bytes
  !----------------------------------------------------------------------------
  call trace_mpi ('win_create_mpi')
  nbytes = 4*nw
  call MPI_Win_create (c, nbytes, 1, MPI_INFO_NULL, MPI_COMM_WORLD, win, mpi_err)
  call assert_mpi ('MPI_Win_create', mpi_err)
END SUBROUTINE win_create_mpi

!*******************************************************************************
SUBROUTINE get_ints_mpi (win, b, nw, offset, rank)
  implicit none
  integer:: win, nw, offset, rank
  real, dimension(nw):: b
  integer(kind=MPI_ADDRESS_KIND):: offset2
  !----------------------------------------------------------------------------
  ! Note that offset is counted in bytes, while numbers are storage units
  !----------------------------------------------------------------------------
  call trace_mpi ('get_real_win')
  offset2 = offset*4
  call MPI_Get (b, nw, MPI_REAL, rank, offset2, nw, MPI_REAL, win, mpi_err)
  call assert_mpi ('MPI_Get', mpi_err)
END SUBROUTINE get_ints_mpi

!*******************************************************************************
SUBROUTINE put_reals_mpi (win, b, nw, offset, rank)
  implicit none
  integer:: win, nw, offset, rank
  real, dimension(nw):: b
  integer(kind=MPI_ADDRESS_KIND):: offset2
  !----------------------------------------------------------------------------
  ! Note that offset is counted in bytes, while numbers are storage units
  !----------------------------------------------------------------------------
  call trace_mpi ('put_real_win')
  offset2 = offset*4
  call MPI_Put (b, nw, MPI_REAL, rank, offset2, nw, MPI_REAL, win, mpi_err)
  call assert_mpi ('MPI_Get', mpi_err)
END SUBROUTINE put_reals_mpi

!*******************************************************************************
SUBROUTINE get_reals_mpi (win, b, nw, offset, rank)
  implicit none
  integer:: win, nw, offset, rank
  real, dimension(nw):: b
  integer(kind=MPI_ADDRESS_KIND):: offset2
  !----------------------------------------------------------------------------
  ! Note that offset is counted in bytes, while numbers are storage units
  !----------------------------------------------------------------------------
  call trace_mpi ('get_real_win')
  offset2 = offset*4
  call MPI_Get (b, nw, MPI_REAL, rank, offset2, nw, MPI_REAL, win, mpi_err)
  call assert_mpi ('MPI_Get', mpi_err)
END SUBROUTINE get_reals_mpi

!*******************************************************************************
SUBROUTINE put_real_mpi (win, b, nw, offset, rank)
  implicit none
  integer:: win, nw, offset, rank
  real, dimension(nw):: b
  integer(kind=MPI_ADDRESS_KIND):: offset2
  !----------------------------------------------------------------------------
  ! Note that offset is counted in bytes, while numbers are storage units
  !----------------------------------------------------------------------------
  call trace_mpi ('put_real_win')
  offset2 = offset*4
  call MPI_Put (b, nw, MPI_REAL, rank, offset2, nw, MPI_REAL, win, mpi_err)
  call assert_mpi ('MPI_Get', mpi_err)
END SUBROUTINE put_real_mpi

!*******************************************************************************
SUBROUTINE win_fence_mpi (win)
  implicit none
  integer:: win
  !----------------------------------------------------------------------------
  ! Sync windows
  !----------------------------------------------------------------------------
  call trace_mpi ('win_fence_mpi')
  call MPI_Win_fence (0, win, mpi_err)
  call assert_mpi ('MPI_Win_fence', mpi_err)
  call trace_mpi ('win_fence_mpi end')
END SUBROUTINE win_fence_mpi

!*******************************************************************************
SUBROUTINE win_free_mpi (win)
  implicit none
  integer:: win
  !----------------------------------------------------------------------------
  ! Assert there is no more window activity, and free the window
  !----------------------------------------------------------------------------
  call trace_mpi ('win_free_mpi')
  call MPI_Win_fence (MPI_MODE_NOSUCCEED, win, mpi_err)
  call MPI_Win_free (win, mpi_err)
  call assert_mpi ('MPI_Win_free', mpi_err)
END SUBROUTINE win_free_mpi

END MODULE mpi_rma
