! $Id$
!*******************************************************************************
MODULE mpi_io_mod
  USE mpi_base
  implicit none
  integer:: handle                                                              ! file handle
  integer:: filetype                                                            ! file type
  integer:: status(MPI_STATUS_SIZE)                                             ! file status
  integer(kind=MPI_OFFSET_KIND):: pos                                           ! position offset into file
  integer(kind=MPI_OFFSET_KIND):: bytesize                                      ! bytes per record
PUBLIC
CONTAINS

!*******************************************************************************
SUBROUTINE file_open_mpi (filename, dd, d, offset, mode)
  implicit none
  integer:: mode
  character(len=*):: filename
  integer:: d(3)                                                                ! local dimensions
  integer:: dd(3)                                                               ! global dimensions
  integer:: offset(3)                                                           ! offset into array
!...............................................................................
  bytesize = 4_8*product(int(dd,kind=8))                                        ! record size in bytes
  CALL MPI_File_open (mpi_comm, filename, mode, MPI_INFO_NULL, handle, mpi_err) ! open file
  CALL MPI_Type_create_subarray (3, dd, d, offset, MPI_ORDER_FORTRAN, &         ! make filetype
                                 MPI_REAL, filetype, mpi_err)   
  CALL MPI_Type_commit (filetype, mpi_err)                                      ! commit filetype
END SUBROUTINE file_open_mpi

!*******************************************************************************
SUBROUTINE file_openw_mpi (filename, dd, d, offset)
  implicit none
  integer, dimension(3):: dd, d, offset
  character(len=*) filename
!...............................................................................
  call file_open_mpi (filename, dd, d, offset, MPI_MODE_CREATE+MPI_MODE_RDWR)   ! open read-write
END SUBROUTINE file_openw_mpi

!*******************************************************************************
SUBROUTINE file_openr_mpi (filename, dd, d, offset)
  implicit none
  integer, dimension(3):: dd, d, offset
  character(len=*) filename
!...............................................................................
  call file_open_mpi (filename, dd, d, offset, MPI_MODE_RDONLY)                 ! open read-only
END SUBROUTINE file_openr_mpi

!*******************************************************************************
SUBROUTINE file_write_mpi (f, d, rec)
  implicit none
  integer:: d(3), rec
  real, dimension(d(1),d(2),d(3)):: f
!...............................................................................
  pos = rec*bytesize                                                            ! position in file
  CALL MPI_File_set_view (handle, pos, MPI_REAL, filetype, 'native', MPI_INFO_NULL, mpi_err)   
  CALL MPI_File_write_all (handle, f , product(d), MPI_REAL, status, mpi_err)   ! collective write
END SUBROUTINE file_write_mpi

!*******************************************************************************
SUBROUTINE file_read_mpi (f, d, rec)
  implicit none
  integer:: d(3), rec
  real, dimension(d(1),d(2),d(3)):: f
!...............................................................................
  pos = rec*bytesize                                                            ! position in file
  CALL MPI_FILE_SET_VIEW (handle, pos, MPI_REAL, filetype, 'native', MPI_INFO_NULL, mpi_err)   
  CALL MPI_FILE_READ_ALL(handle, f , product(d), MPI_REAL, status, mpi_err)     ! collective read
END SUBROUTINE file_read_mpi

!*******************************************************************************
SUBROUTINE file_close_mpi
  implicit none
!...............................................................................
  CALL MPI_File_close (handle, mpi_err)                                         ! close the file
END SUBROUTINE file_close_mpi

END MODULE mpi_io_mod
