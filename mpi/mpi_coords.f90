!*******************************************************************************
MODULE mpi_coords
  USE mpi_base
  implicit none
PUBLIC
  logical:: mpi_reorder         ! Reordering of ranks allowed, or not
  logical:: mpi_periodic(3)     ! Periodic MPI dimensions, or not
  integer:: mpi_dims(3)         ! Cartesian MPI dimensions
  integer:: mpi_coord(3)        ! Our cordinates in Cartesian MPI space
  integer:: mpi_plane(3)        ! Communicator btw ranks in planes
  integer:: mpi_beam(3)         ! Communicator btw ranks along axis directions
  integer:: mpi_dn(3)           ! Ranks downwards (circular)
  integer:: mpi_up(3)           ! Ranks upwards (circular)
CONTAINS

!*******************************************************************************
SUBROUTINE cart_create_mpi (mpi_dim)
  implicit none
  integer, dimension(3):: mpi_dim
  integer:: i, color, cart, size
  character(len=mch), save:: id= &
    'mpi_coords.f90 $Id$'
!-------------------------------------------------------------------------------
  mpi_dims = mpi_dim
  mpi_reorder = .true.
  mpi_periodic = .true.
  if (master) print'(a,3i4)',' Using Cartesian MPI mesh with dimensions',mpi_dims
  !-----------------------------------------------------------------------------
  ! Create a communicator (cart) with Cartesian ordering
  !-----------------------------------------------------------------------------
  if (mpi_size /= product(mpi_dims)) then
     if (master) print *,'ERROR: mpi_size must be =', product(mpi_dims)
     call end_mpi
  end if
  call MPI_Cart_create (mpi_comm_world, 3, mpi_dim, mpi_periodic, mpi_reorder, &
                        cart, mpi_err)
  call assert_mpi ('cart_creat_mpi: cart', mpi_err)
  !-----------------------------------------------------------------------------
  ! Compute mpi_coord; our coordinates in Cartesian MPI space
  !-----------------------------------------------------------------------------
  call MPI_Cart_coords (cart, mpi_rank, 3, mpi_coord, mpi_err)                  ! mpi_coord(i) = mpi_[xyz]
  call assert_mpi ('cart_creat_mpi: mpi_coord', mpi_err)
  do i=1,3
    !---------------------------------------------------------------------------
    ! Compute mpi_up and mpi_dn; the ranks of nearest neighbors
    !---------------------------------------------------------------------------
    call MPI_Cart_shift (cart, i-1, 1, mpi_dn(i), mpi_up(i), mpi_err)           ! mpi_{up,dn} ranks
    call assert_mpi ('cart_creat_mpi: mpi_dn/up', mpi_err)
    !---------------------------------------------------------------------------
    ! Compute mpi_plane; the communicators for planes in MPI-space
    !---------------------------------------------------------------------------
    call MPI_Comm_split (mpi_comm_world, mpi_coord(i), mpi_rank, mpi_plane(i),& ! mpi_plane(
                         mpi_err)
    call assert_mpi ('cart_creat_mpi: mpi_plane', mpi_err)
    !---------------------------------------------------------------------------
    ! Compute mpi_beam; the communicators along MPI coordinat axes
    !---------------------------------------------------------------------------
    color = merge(0,mpi_coord(1),i==1) &
          + merge(0,mpi_coord(2),i==2)*mpi_dim(1) &
          + merge(0,mpi_coord(3),i==3)*mpi_dim(1)*mpi_dim(2)
    call MPI_Comm_split (mpi_comm_world, color, mpi_coord(i), mpi_beam(i), mpi_err)
    call assert_mpi ('cart_creat_mpi: mpi_beam', mpi_err)
  end do
END SUBROUTINE cart_create_mpi

END MODULE mpi_coords
