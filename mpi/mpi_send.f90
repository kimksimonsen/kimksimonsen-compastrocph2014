! $Id$
!*******************************************************************************
MODULE mpi_send
  USE mpi_base
  implicit none 
PUBLIC
CONTAINS

!*******************************************************************************
SUBROUTINE bcast_ints_mpi (a, nw, rank, mpi_com)
  implicit none
  integer:: nw, rank, mpi_com
  real:: a(nw)
  call MPI_Bcast (a, nw, MPI_INT, rank, mpi_com, mpi_err)
  call assert_mpi ('bcast_ints_mpi', mpi_err)
END SUBROUTINE bcast_ints_mpi

!*******************************************************************************
SUBROUTINE bcast_reals_mpi (a, nw, rank, mpi_com)
  implicit none
  integer:: nw, rank, mpi_com
  real:: a(nw)
  call MPI_Bcast (a, nw, MPI_REAL, rank, mpi_com, mpi_err)
  call assert_mpi ('bcast_reals_mpi', mpi_err)
END SUBROUTINE bcast_reals_mpi

!*******************************************************************************
SUBROUTINE isend_ints_mpi (a, nw, rank, mpi_com, req)
  implicit none
  integer:: nw, rank, mpi_com, req
  integer:: a(nw)
  call MPI_Isend (a, nw, MPI_INT, rank, mpi_rank, mpi_com, req, mpi_err)
  call assert_mpi ('isend_reals_mpi', mpi_err)
END SUBROUTINE isend_ints_mpi

!*******************************************************************************
SUBROUTINE irecv_ints_mpi (a, nw, rank, mpi_comm, req)
  implicit none
  integer:: nw, rank, mpi_comm, req
  integer:: a(nw)
  call MPI_Irecv (a, nw, MPI_INT, rank, rank, mpi_comm, req, mpi_err)
  call assert_mpi ('irecv_reals_mpi', mpi_err)
END SUBROUTINE irecv_ints_mpi

!*******************************************************************************
SUBROUTINE isend_reals_mpi (a, nw, rank, mpi_com, req)
  implicit none
  integer:: nw, rank, mpi_com, req
  real:: a(nw)
  call MPI_Isend (a, nw, MPI_REAL, rank, mpi_rank, mpi_com, req, mpi_err)
  call assert_mpi ('isend_reals_mpi', mpi_err)
END SUBROUTINE isend_reals_mpi

!*******************************************************************************
SUBROUTINE irecv_reals_mpi (a, nw, rank, mpi_comm, req)
  implicit none
  integer:: nw, rank, mpi_comm, req
  real:: a(nw)
  call MPI_Irecv (a, nw, MPI_REAL, rank, rank, mpi_comm, req, mpi_err)
  call assert_mpi ('irecv_reals_mpi', mpi_err)
END SUBROUTINE irecv_reals_mpi

!*******************************************************************************
SUBROUTINE wait_mpi (req)
  implicit none
  integer:: req
  integer:: status(MPI_STATUS_SIZE)
  call MPI_Wait (req, status, mpi_err)
  call assert_mpi ('wait_mpi', mpi_err)
END SUBROUTINE wait_mpi

!*******************************************************************************
SUBROUTINE waitall_mpi (req, n)
  implicit none
  integer:: n, req(n)
  call MPI_Waitall (n, req, MPI_STATUSES_IGNORE, mpi_err)
  call assert_mpi ('waitall_mpi', mpi_err)
END SUBROUTINE waitall_mpi

END MODULE mpi_send
