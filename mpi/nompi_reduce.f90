! $Id$
!*******************************************************************************
MODULE mpi_reduce
PUBLIC
CONTAINS

!***********************************************************************
SUBROUTINE min_int_mpi (a)
  implicit none
  integer:: a
!-----------------------------------------------------------------------
END SUBROUTINE

!***********************************************************************
SUBROUTINE max_int_mpi (a)
  implicit none
  integer:: a
!-----------------------------------------------------------------------
END SUBROUTINE

!***********************************************************************
SUBROUTINE min_real_mpi (a)
  implicit none
  real:: a
!-----------------------------------------------------------------------
END SUBROUTINE

!***********************************************************************
SUBROUTINE max_real_mpi (a)
  implicit none
  real:: a
!-----------------------------------------------------------------------
END SUBROUTINE

!***********************************************************************
SUBROUTINE min_ints_mpi (a, n)
  implicit none
  integer:: n
  integer:: a(n)
!-----------------------------------------------------------------------
END SUBROUTINE

!***********************************************************************
SUBROUTINE max_ints_mpi (a, n)
  implicit none
  integer:: n
  integer:: a(n)
!-----------------------------------------------------------------------
END SUBROUTINE

!***********************************************************************
SUBROUTINE min_reals_mpi (a, n)
  implicit none
  integer:: n
  real:: a(n)
!-----------------------------------------------------------------------
END SUBROUTINE

!***********************************************************************
SUBROUTINE max_reals_mpi (a, n)
  implicit none
  integer:: n
  real:: a(n)
!-----------------------------------------------------------------------
END SUBROUTINE

END MODULE mpi_reduce
