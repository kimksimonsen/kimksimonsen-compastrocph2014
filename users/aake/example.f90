!*******************************************************************************
PROGRAM example
  USE mpi_rma
  implicit none
  integer, parameter:: nw=100
  real, dimension(nw):: a, b
  integer i, win, rank, offset
  character(len=mch), save:: id= &
    'example.f90 $Id$'
!-------------------------------------------------------------------------------
  call init_mpi
  call print_id (id)
  call barrier_mpi('init')
!-------------------------------------------------------------------------------
  call win_create_mpi (win, a, nw)                                              ! make a available
  do i=1,nw
    a(i) = i + nw*mpi_rank
  end do
!-------------------------------------------------------------------------------
  rank = mod(mpi_rank+1, mpi_size)                                              ! get from one rank up
  offset = 10
  call win_fence_mpi (win)                                                      ! <-  NO change to ..
  call get_reals_mpi (win, b, nw-offset, offset, rank)                           !   | .. a in this ..
  call win_fence_mpi (win)                                                      ! <-  .. interval
  print *,mpi_rank,b(1)                                                         ! check results
!-------------------------------------------------------------------------------
  call win_free_mpi (win)
  call end_mpi
END PROGRAM example
